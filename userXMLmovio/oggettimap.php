

<?php








$host = "localhost";
$db = "movio";
$user = "root";
$password = "";


try {
  
  $connessione = new PDO("mysql:host=$host;dbname=$db", $user, $password);
  
  $connessione->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  $crea_tb = $connessione->exec("CREATE TABLE `mappeoggetti` (
  `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  `nome` VARCHAR( 60 ) NOT NULL ,
  `indirizzo` VARCHAR( 80 ) NOT NULL ,
  `latitudine` FLOAT( 10, 6 ) NOT NULL ,
  `longitudine` FLOAT( 10, 6 ) NOT NULL ,
  `tiporeperto` VARCHAR( 30 ) NOT NULL
) ENGINE = MYISAM");
  
  $connessione = null;
}
catch(PDOException $e)
{
  
  echo $e->getMessage();
}
?>



 

